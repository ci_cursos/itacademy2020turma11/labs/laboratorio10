﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace laboratorio10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Pessoa> pessoas = new List<Pessoa>
            {
                new Pessoa {Nome="Ana", DataNascimento= new DateTime(1980,3,14), Casada=true},
                new Pessoa {Nome="Paulo", DataNascimento= new DateTime(1978,10,23), Casada=true},
                new Pessoa {Nome="Maria", DataNascimento= new DateTime(2000,1,10), Casada=false},
                new Pessoa {Nome="Carlos", DataNascimento= new DateTime(1999,12,12), Casada=false}
            };
            /*
            var linq1 = from p in pessoas
                        where p.Casada
                        select p;
            linq1.ToList().ForEach(p => Console.WriteLine(p));

            var linq2 = pessoas.Where(p => p.Casada);
            linq2.ToList().ForEach(p => Console.WriteLine(p));
            */
            /*
            var linq3 = from p in pessoas
                        where p.Casada && p.DataNascimento >= new DateTime(1980,1,1)
                        select p;
            foreach(var p in linq3)
            {
                Console.WriteLine(p);
            }

            var linq4 = pessoas.Where(p => p.Casada && p.DataNascimento >= new DateTime(1980,1,1));
            foreach(var p in linq4)
            {
                Console.WriteLine(p);
            }
            */
            /*
            var linq5 = from p in pessoas
                        where p.Casada
                        select p.Nome;
            var linq6 = pessoas.Where(p => p.Casada).Select(p => p.Nome);
            */
            /*
            var linq7 = from p in pessoas
                        orderby p.DataNascimento, p.Nome
                        select p;
            var linq8 = pessoas.OrderBy(p => p.DataNascimento).ThenBy(p => p.Nome);

            linq8.ToList().ForEach(p => Console.WriteLine(p));
            */
            /*
            var linq9 = from p in pessoas
                        group p by p.Casada;
            foreach(var agrupamento in linq9)
            {
                if (agrupamento.Key)
                {
                    Console.WriteLine("Casadas:");
                }
                else
                {
                    Console.WriteLine("Solteiras:");
                }
                foreach (var pessoa in agrupamento)
                {
                    Console.WriteLine(pessoa);
                }
            }
            */
            var linq10 = pessoas.Where(p => p.Casada).Count();
            Console.WriteLine($"Total de casadas: {linq10}");
        }
    }
}
